﻿var geoCoder = new google.maps.Geocoder();
var lat;
var long;
var locationInput;
var weatherAPI = '698ddca09568d2ac0e88dfadc34e40a1/';
var storageIndex = 0;


function getCoordsHist() {
    if (document.getElementById('locHistory').value == "") {
        alert("Please enter a location");
        return;
    }
    
    var locationInput = document.getElementById('locHistory').value;
    
    localStorage.setItem('' + storageIndex, locationInput);
    storageIndex++;
    geoCoder.geocode({ address: locationInput }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            lat = results[0].geometry.location.lat();
            long = results[0].geometry.location.lng();
        }
        getHistory();
    }
)
}

function getSearchHistory() {
    var sh = "";
    for (var i = 0; i < localStorage.length; i++) {
        sh = sh + " " + localStorage.getItem('' + i);
        
    }
    
    document.getElementById('history').innerHTML = sh;
}

function load() {
    var selDay = document.getElementById('day');
    var selYear = document.getElementById('year');
    var date = new Date();
    for (var i = 1; i <= 31; i++) {
        var optDay = document.createElement('option');
        optDay.innerHTML = i;
        optDay.value = i;
        selDay.appendChild(optDay);
    }
    for (var i = 1970; i <= date.getFullYear() ; i++) {
        var optYear = document.createElement('option');
        optYear.innerHTML = i;
        optYear.value = i;
        selYear.appendChild(optYear);


    }

    
    



};

function timestamp(sDate) {
    var uTime = Date.parse(sDate);
    return uTime / 1000;
}

function getHistory() {
    var mIdx = document.getElementById("month").selectedIndex;
    var dIdx = document.getElementById("day").selectedIndex;
    var yIdx = document.getElementById("year").selectedIndex;
    var unixTime;
    month = document.getElementById("month").options[mIdx].text;
    day = document.getElementById("day").options[dIdx].text;
    year = document.getElementById("year").options[yIdx].text;
    unixTime = timestamp(year + " " + month + " " + day + " " + "00:00:00");

    $.ajax({
        url: "https://api.forecast.io/forecast/" + weatherAPI + lat + "," + long + "," + unixTime,
        jsonp: "callback",
        dataType: "jsonp",
        success: function (data) {

            drawTempChart(data);
            drawWindChart(data);
            drawHumidChart(data);
        }
    })
};

function drawHumidChart(data) {
    var dataT = new google.visualization.DataTable();
    dataT.addColumn('number', 'hour');
    dataT.addColumn('number', 'Humidity %');

    for (var i = 0; i < 24; i++) {
        dataT.addRows([[i, parseInt(data.hourly.data[i].humidity) * 100]]);
    }
    var options = {
        chart: {
            title: 'Humidity variation throughout the day'
            
        },
        height: 400
    }
    var chart = new google.charts.Line(document.getElementById('humidGraph'));
    chart.draw(dataT, options);
}

function drawWindChart(data) {
    var dataT = new google.visualization.DataTable();
    dataT.addColumn('number', 'hour');
    dataT.addColumn('number', 'Wind Speed (mph)');

    for (var i = 0; i < 24; i++) {
        if(data.hourly.data[i].windSpeed != null)
            dataT.addRows([[i, parseInt(data.hourly.data[i].windSpeed)]]);
        else
            dataT.addRows([[i, 0]]);
    }
    var options = {
        chart: {
            title: 'Wind speed variation throughout the day',
            subtitle: 'in miles per hour',
        },
        height: 400 
    }
    var chart = new google.charts.Line(document.getElementById('windGraph'));
    chart.draw(dataT, options);
}

function drawTempChart(data) {

    var dataT = new google.visualization.DataTable();
    dataT.addColumn('number', 'hour');
    dataT.addColumn('number', 'Temperature (F)');
    
    for (var i = 0; i < 24; i++) {
        dataT.addRows([[i, parseInt(data.hourly.data[i].temperature)]]);
    }
    var options = {
        chart: {
            title: 'Temperature variation throughout the day',
            subtitle: 'in Fahrenheit',
            
        },

        height: 400,
        
        }
    var chart = new google.charts.Line(document.getElementById('coords'));
    chart.draw(dataT, options);
}