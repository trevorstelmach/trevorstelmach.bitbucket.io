﻿var geoCoder = new google.maps.Geocoder();


var skycons = new Skycons({ "color": "black" });
var weatherAPI = '698ddca09568d2ac0e88dfadc34e40a1/';
var storageIndex = localStorage.length;


function getSearchHistory() {
    var sh = "";
    
    for (var i = 0; i < localStorage.length; i++) {
        sh = sh + " " + localStorage.getItem('' + i);

    }

    document.getElementById('history').innerHTML = sh;
}
function getCoords() {
    var lat;
    var long;
    if (document.getElementById('location').value == "") {
        alert("Please enter a location");
        return;
    }

    var locationInput = document.getElementById('location').value;
    localStorage.setItem('' + storageIndex, locationInput);
    storageIndex++;
    geoCoder.geocode({ address: locationInput }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            lat = results[0].geometry.location.lat();
            long = results[0].geometry.location.lng();
        }
        getWeather();
        

    })

    function getWeather() {
        $.ajax({
            url: "https://api.forecast.io/forecast/" + weatherAPI + lat + "," + long,
            jsonp: "callback",
            dataType: "jsonp",
            success: function (data) {

                skycons.add("nowIcon", data.currently.icon);

                document.getElementById('summary').innerHTML = "The weather in " + document.getElementById('location').value + " is " + data.currently.summary;
                document.getElementById('realTemp').innerHTML = "Temperature: " + data.currently.temperature + " ˚F";
                document.getElementById('humidity').innerHTML = "Humidity: " + data.currently.humidity * 100 + "%";
                document.getElementById('windSpeed').innerHTML = "Wind speed: " + data.currently.windSpeed + " mph";
                document.getElementById('weekSummary').innerHTML = data.daily.summary;
                document.getElementById('weekTitle').innerHTML = 'This weeks weather summary:';
                document.getElementById('currentLabel').innerHTML = 'Current weather:';

                for (var i = 0; i < 7; i++) {
                    skycons.add("nowIcon" + i, data.daily.data[i].icon);
                    document.getElementById('date' + i).innerHTML = timeConverter(data.daily.data[i].time);
                    document.getElementById('summary' + i).innerHTML = data.daily.data[i].summary;
                    document.getElementById('hiTemp' + i).innerHTML = "High: " + data.daily.data[i].temperatureMax + " ˚F";
                    document.getElementById('loTemp' + i).innerHTML = "Low: " + data.daily.data[i].temperatureMin + " ˚F";
                    document.getElementById('pChance' + i).innerHTML = "Chance of rain: " + data.daily.data[i].precipProbability * 100 + "%";

                }
            }
        });

    }
    function timeConverter(UNIX_timestamp) {
        var a = new Date(UNIX_timestamp * 1000);
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var day = days[a.getDay()];
        var year = a.getFullYear();
        var month = months[a.getMonth()];
        var date = a.getDate();
        var time = day + ", " + date + ' ' + month + ' ' + year;
        return time;
    }



}