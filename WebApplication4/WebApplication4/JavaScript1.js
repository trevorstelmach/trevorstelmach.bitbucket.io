﻿var classApp = angular.module('weatherApp', []);

classApp.controller("weatherCtrl", function ($scope, $http) {
    var vm = $scope;


    vm.channelInfo = {
        heading: "Open Weather API",
        subheading1: "Trevor Stelmach",
        subheading2: {
            name: "Check out my portfolio",
            link: "http://www.trevorstelmach.com"
        }
    };
    $http.get("https://ipapi.co/json").success(function(data) {
    console.log(data);
    vm.lat = data.latitude;
    vm.lon = data.longitude;
    
    var apiKey = "c132088c280665571d393ff0207e7aac";
    var openWeatherURL =
      "https://api.openweathermap.org/data/2.5/weather?lat=" +
      vm.lat +
      "&lon=" +
      vm.lon +
      "&APPID=" +
      apiKey;

        $http.get(openWeatherURL).success(function (data) {
            vm.description = data.weather[0].description;
            vm.speed = (2.237 * data.wind.speed).toFixed(1) + " mph";
            vm.name = data.name;
            vm.temp = data.main.temp;
            vm.fTemp = (vm.temp * (9 / 5) - 459.67).toFixed(1) + " Degrees F";
            vm.cTemp = (vm.temp - 273).toFixed(1) + " Degrees C";
            vm.icon = "http://www.openweathermap.org/img/w/" + data.weather[0].icon + ".png";
            switch (vm.description) {
                case 'clear sky': {
                    vm.weatherBackground = {
                        "background": "url('http://1.bp.blogspot.com/_mtfESbBl8k8/THWJ4Zscr9I/AAAAAAAAAXs/Yr6iqzedbus/s1600/grama.jpg')",
                        "background-size": "cover" 
                    };
                    break;
                }
                case 'broken clouds': {
                    vm.weatherBackground = {
                        "background": "url('http://img13.deviantart.net/fa39/i/2015/052/6/1/broken_clouds_by_leo_6tos-d8ixdlv.jpg')",
                        "background-size": "cover"
                    };
                    break;
                }
                case 'few clouds': {
                    vm.weatherBackground = {
                        "background": "url('https://crystalseye.files.wordpress.com/2011/08/dsc_0724.jpg')",
                        "background-size": "cover"
                    };
                    break;
                }
                case 'mist': {
                    vm.weatherBackground = {
                        "background": "url('https://www.muralswallpaper.com/app/uploads/misty-rainforest-plain-820x532.jpg')",
                        "background-size": "cover"
                    };
                    break;
                }
                case 'rain': {
                    vm.weatherBackground = {
                        "background": "url('https://images.alphacoders.com/120/thumb-1920-120313.jpg')",
                        "background-size": "cover"
                    };
                    break;
                }
                case 'scattered clouds': {
                    vm.weatherBackground = {
                        "background": "url('http://wallpapercave.com/wp/iHUonse.jpg')",
                        "background-size": "cover"
                    };
                    break;
                }
                case 'shower rain': {
                    vm.weatherBackground = {
                        "background": "url('http://cdn.wallpapersafari.com/89/43/V56CWU.jpg')",
                        "background-size": "cover"
                    };
                    break;
                }
                case 'snow': {
                    vm.weatherBackground = {
                        "background": "url('http://wallpapercave.com/wp/9uTVIpA.jpg')",
                        "background-size": "cover"
                    };
                    break;
                }                   
                case 'thunderstorm': {
                        vm.weatherBackground = {
                            "background": "url(' http://wallpapercave.com/wp/FTvkLn5.jpg')",
                            "background-size": "cover"
                        };
                        break;
                    }
                default:
                    vm.weatherBackground = {
                        "background": "url('https://puserscontentstorage.blob.core.windows.net/userimages/794eddda-eb5c-4f44-aab6-18b49922f8d4/a8093ce4-f09c-4590-a8e9-2ca479e866c3image1.gif')",
                        "background-size": "cover"
                    };
                    break;
            }
        });

    });

});